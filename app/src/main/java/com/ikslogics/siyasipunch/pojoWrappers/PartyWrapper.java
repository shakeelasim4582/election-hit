package com.ikslogics.siyasipunch.pojoWrappers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.ikslogics.siyasipunch.pojo.Party;

public class PartyWrapper {
    private Party party;
    private Bitmap partyFlag;
    private Bitmap partyLogo;


    public PartyWrapper(Party party) {
        this.party = party;
    }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public Bitmap getPartyFlag() {
        return partyFlag;
    }

    public Bitmap getPartyLogo() {
        return partyLogo;
    }

    public void populateBitmaps(Context context) {
        partyFlag = BitmapFactory.decodeFile(context.getFilesDir().getAbsolutePath() + "/" + party.getFlag());
        partyLogo = BitmapFactory.decodeFile(context.getFilesDir().getAbsolutePath() + "/" + party.getLogo());
    }
    public Drawable getLocalDrawableFlag(Context context ){
        String flagImageName = party.getFlag().substring(0, party.getFlag().lastIndexOf("."));
        return context.getResources().getDrawable(context.getResources().getIdentifier(flagImageName , "drawable", context.getPackageName()));
    }
    public Bitmap getLocalSelectedFlagBitMap(Context context){
        String flagImageName = party.getFlag().substring(0, party.getFlag().lastIndexOf(".")) + "_h";
        Drawable drawable = context.getResources().getDrawable(context.getResources().getIdentifier(flagImageName , "drawable", context.getPackageName()));
        return ((BitmapDrawable)drawable).getBitmap();
    }


}
