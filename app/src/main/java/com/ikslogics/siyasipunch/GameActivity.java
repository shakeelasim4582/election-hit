package com.ikslogics.siyasipunch;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.ikslogics.siyasipunch.pojo.ElectionStore;
import com.ikslogics.siyasipunch.pojo.Hider;
import com.ikslogics.siyasipunch.pojo.Member;
import com.ikslogics.siyasipunch.pojo.Party;
import com.ikslogics.siyasipunch.pojo.Poster;
import com.ikslogics.siyasipunch.pojo.Scene;
import com.ikslogics.siyasipunch.pojoWrappers.PartyWrapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class GameActivity extends Activity  {
    ArrayList<ImageView> imagesList = new ArrayList<ImageView>();
    ArrayList<ImageView> lifeImagesList = new ArrayList<ImageView>();
    ArrayList<PartyMemberWrapper> selectedPartyMembers = new ArrayList<PartyMemberWrapper>();
    TextView score;
    TextView highScore;
    MediaPlayer punchSound;
    MediaPlayer gruntSound;
    SharedPreferences sharedPreferences;
    MediaPlayer myMediaPlayer;
    private InterstitialAd mInterstitialAd;
    private Class<?> nextActivity = PosterActivity.class;
    boolean isSoundOn ;
    int zero = 0;
    int timerDay = 500;
    int timerPeriod = 500;
    int mediaPlayerLenght ;
    RelativeLayout layoutRootView;
    int screenHeight  ;
    int screenWidth ;
    boolean isHighScore = true;
    Timer timer;
    AlertDialog alertDialog;


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);

        sharedPreferences = getSharedPreferences("election_hit", Activity.MODE_PRIVATE);
        isSoundOn = sharedPreferences.getBoolean("isSoundOn",true);
        initPartyFlags();
        restScore();   // Reset Score to initial state
        initAds();      // initialize Admob ads
        createHittingSounds();  //creating hitting sounds of characters
        startBackgroundMusic(); //creating and starting background music
        initLifeView();  //   initialize characters life view
        setInitialScores(); // setting initial score and most recent High Score
        InitGameSceneAndMembers(); // initializing game scene and selecting Random members for scene
        initializeTimer(); //initializing timer for Game

    }

    private void startBackgroundMusic(){
        Party party = AppStateController.getInstance().getSelectedParty();
        Random random = new Random();
        int randomMusic = random.nextInt(party.getBackgroundMusic().size());
        String url = party.getBackgroundMusic().get(randomMusic).getMusic();
        myMediaPlayer = new MediaPlayer();
        myMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        myMediaPlayer.setLooping(true);
        try {
            myMediaPlayer.setDataSource(url);
            myMediaPlayer.prepareAsync(); // might take long! (for buffering, etc)

        } catch (IOException e) {
//            Toast.makeText(this, "mp3 not found", Toast.LENGTH_SHORT).show();
//            e.printStackTrace();
        }
        //mp3 will be started after completion of preparing...
        myMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer player) {
                player.setLooping(true);
                if (sharedPreferences.getBoolean("isMusicOn", true)) {
                    player.start();
                }
            }
        });


    }

    private void selectMembersForGame() {
        ElectionStore electionStore = AppStateController.getInstance().getElectionStore();
        for (Party party : electionStore.getParties()) {
            if (party.getPID() == AppStateController.getInstance().getSelectedParty().getPID() ||
                    party.getPID() == AppStateController.getInstance().getOpponentParty().getPID()
                    || party.getPID() == 4 ) {
                for (Member partyMember : party.getMembers()) {
                    PartyMemberWrapper partyMemberWrapper = new PartyMemberWrapper(partyMember);
                    partyMemberWrapper.populateBitmaps(this);
                    partyMemberWrapper.setPartyId(party.getPID());
                    selectedPartyMembers.add(partyMemberWrapper);
                }
            }
        }
    }

    private void timerClick() {

        Random random = new Random();
        int randomIV = random.nextInt(imagesList.size());
        ImageView characterIV = imagesList.get(randomIV);
        CharacterState characterState = (CharacterState) characterIV.getTag(R.string.character_state_tag);
        if (!characterState.isMoving) {
            //Party selectedParty = AppStateController.getInstance().getSelectedParty();
            int randomMember = random.nextInt(selectedPartyMembers.size());
            PartyMemberWrapper partyMember = selectedPartyMembers.get(randomMember);
            characterIV.setTag(R.string.character_hit, partyMember);

            if (partyMember.getBitmapImages().size() <= partyMember.getHitCount()) {
                partyMember.hitCount = zero;
            }
            Bitmap characterBitmap = (Bitmap) partyMember.getBitmapImages().get(partyMember.getHitCount());
            if ( characterBitmap == null){
                Drawable characterImage = getMemberDrawableImage(partyMember.getMemberPojo().getImages().get(partyMember.getHitCount()).getImage());
                //characterIV.setImageDrawable(characterImage);
                characterBitmap = ((BitmapDrawable)characterImage).getBitmap();
            }
            characterIV.setImageBitmap(characterBitmap);
            characterIV.setClickable(false);
            objectAnimation(characterIV);
        }


    }

    private void objectAnimation(ImageView imageView) {

        int currentScore = AppStateController.getInstance().getScore() <= 0 ? 1:AppStateController.getInstance().getScore();
        int duration = 1000 - (1000*(currentScore/6)/100);
        final ObjectAnimator translateYAnimation = ObjectAnimator.ofFloat(imageView, "translationY", -(imageView.getHeight()*(imageView.getScaleY()))); // - imageView.getScaleY()*(10*AppStateController.getInstance().densityScaleFactor))
        translateYAnimation.setRepeatCount(1);//ValueAnimator.INFINITE
        translateYAnimation.setRepeatMode(ValueAnimator.REVERSE);
        translateYAnimation.setDuration(duration);
        CharacterState characterState = (CharacterState) imageView.getTag(R.string.character_state_tag);
        characterState.isMoving = true;
        translateYAnimation.start();
        translateYAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                ImageView characterIV = (ImageView) translateYAnimation.getTarget();
                CharacterState characterState = (CharacterState) characterIV.getTag(R.string.character_state_tag);
                characterState.isMoving = false;

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        float touchX = (Float) motionEvent.getX();
        float touchY = (Float) motionEvent.getY();
        Log.i("TAG", "touch");
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                Log.i("TAG", "touched down");
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                Log.i("TAG", "touched up");
                for (int i = zero; i < imagesList.size(); i++) {
                    ImageView characterImageV = imagesList.get(i);
                    PartyMemberWrapper characterHit = (PartyMemberWrapper) characterImageV.getTag(R.string.character_hit);
                    CharacterState characterState = (CharacterState) characterImageV.getTag(R.string.character_state_tag);
                    if (characterState.isMoving) {
                        int[] characterPosition = {zero, zero};
                        characterImageV.getLocationOnScreen(characterPosition);
                        float imageX = characterPosition[zero];
                        float imageY = characterPosition[1];
                        int imageHeight = characterImageV.getHeight();
                        int imageWidth = characterImageV.getWidth();

                        if ((touchX >= imageX && touchX <= imageX + imageWidth) && (touchY >= imageY && touchY <= imageY + imageHeight)) {
                            Log.i("TAG", "touched found  **********************************************");
                            characterImageV.setClickable(true);
                            characterHit.setHitCount(1);
                            updateScore(characterHit,imageX,imageY);
                            characterImageV.setImageDrawable(getResources().getDrawable(R.drawable.img_pow));
                            break;
                        }
                    }
                }
                break;
        }

        return true;


        //return false;
    }

    private void updateScore(PartyMemberWrapper characterHit,float imageX, float imageY) {
        if (characterHit.getPartyId() == AppStateController.getInstance().getSelectedParty().getPID()) {

            if (isSoundOn){
                gruntSound.start();
            }
            AppStateController.getInstance().setPartyHitCount(-1);
            manageLifes();
            Vibrator vb = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vb.vibrate(300);
            if (AppStateController.getInstance().getPartyHitCount() == zero) {

                if (AppStateController.getInstance().getScore() <
                        AppStateController.getInstance().getElectionStore().getConfigurations().getBannerScore()){
                    nextActivity = PartySelection.class;
                    timer.cancel();
                    myMediaPlayer.stop();
                    showInfoDialogBox();
                    vb.vibrate(800);

                }
                else {
                    nextActivity = PosterActivity.class;
                    endGame();
                    vb.vibrate(700);
                }
            }

        } else {

            if (isSoundOn){
                punchSound.start();
            }
            String hitScore = "+"+String.valueOf(characterHit.getMemberPojo().getDestroyScore());
            showAnimatedScore(hitScore,imageX,imageY);
            AppStateController.getInstance().setScore(characterHit.getMemberPojo().getDestroyScore());
            score.setText( String.valueOf(AppStateController.getInstance().getScore()));

        }
        if (AppStateController.getInstance().getScore() > sharedPreferences.getInt("highScore", zero)) {
            AppStateController.getInstance().setHighScore(AppStateController.getInstance().getScore());
            highScore.setText(String.valueOf(AppStateController.getInstance().getHighScore()));

            if (isHighScore){
                String highScore = "HighScore"+ String.valueOf(AppStateController.getInstance().getHighScore());
                showHighScoreAnimation("HighScore");
                isHighScore = false;
            }
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("highScore", AppStateController.getInstance().getHighScore());
            editor.commit();

        }

    }

    private void endGame() {
        myMediaPlayer.stop();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            finish();
            startActivity(new Intent(GameActivity.this, nextActivity));
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        new AlertDialog.Builder(this)
                .setTitle("Exit Game ?")
                .setMessage("Do you really want to Exit Game play ?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        nextActivity = PartySelection.class;
                        endGame();
                        //startActivity(new Intent(GameActivity.this,PartySelection.class));
                    }})
                .setNegativeButton(android.R.string.no, null).show();

    }

    private void initLifeView(){

        for (int i=zero; i<=2;i++) {
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.life_layout);
            ImageView lifeImage = new ImageView(getApplicationContext());
            lifeImage.setImageDrawable(getResources().getDrawable(R.drawable.life_heart));
            linearLayout.addView(lifeImage);
            lifeImage.setTag("active");
            lifeImagesList.add(lifeImage);
            LinearLayout.LayoutParams lifeImageParms = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            lifeImageParms.setMargins(15,15,zero,zero);
            lifeImage.setLayoutParams(lifeImageParms);

        }

    }

    private void  manageLifes(){
        for (int i = lifeImagesList.size()-1 ; i >= zero ;i--){
            ImageView lifeImage = lifeImagesList.get(i);
            if (lifeImage.getTag().toString().equals("active")){
                lifeImage.setImageDrawable(getResources().getDrawable(R.drawable.life_dead));
                lifeImage.setTag("dead");
                break;
            }
        }
    }

    private void initAds(){
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.adMob_interstatial_ad_unit_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the interstitial ad is closed.
                finish();
                startActivity(new Intent(GameActivity.this, nextActivity));
            }
        });
        
    }

    private void restScore(){
        AppStateController.getInstance().partyHitCount = 3;
        AppStateController.getInstance().score = zero;
    }

    private void createHittingSounds(){
        punchSound = MediaPlayer.create(this, R.raw.punch_sound);
        gruntSound = MediaPlayer.create(this, R.raw.male_grunt);

    }

    @SuppressLint("NewApi")
    private void InitGameSceneAndMembers(){
        layoutRootView = (RelativeLayout) findViewById(R.id.rootView);
        Scene scene = selectScene();// AppStateController.getInstance().getElectionStore().getScenes().get(0);
        Bitmap rootBitmap = BitmapFactory.decodeFile(getFilesDir().getAbsolutePath() + "/" + scene.getBackgroundImage()) ;
        if (rootBitmap == null){
            layoutRootView.setBackground(getResources().getDrawable(R.drawable.game_bg));
        }else {
            Drawable d = new BitmapDrawable(getResources(), rootBitmap);
            layoutRootView.setBackground(d);
        }
         // setting scene background Image

        selectMembersForGame(); // selecting members for Game

        DisplayMetrics displayMetrics = new DisplayMetrics();   //getting screen width and height
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenHeight = displayMetrics.heightPixels;
        screenWidth = displayMetrics.widthPixels;

        int x;
        int y ;
        int characterImageX;
        int characterImageY;
        float densityScaleFactor = AppStateController.getInstance().densityScaleFactor;
        //Bitmap characterBitmapForCalculation = (Bitmap) selectedPartyMembers.get(0).getBitmapImages().get(0);
        //Hider defaultHider = scene.getHiders().get(10);
        for (Hider hider: scene.getHiders()) {
//            if (hider.getHID() != 5)
//                continue;
            //creating Image and its bitmap of scene Hider Image
            //Screen height and with percentage
            x = (int)(hider.getX() *  screenWidth)/100;
            y = (int)(hider.getY()* screenHeight)/100;

            ImageView barrierImage = new ImageView(getApplicationContext());
            Bitmap barrierBitmap =BitmapFactory.decodeFile(getFilesDir().getAbsolutePath() + "/" + hider.getImage());
            if (barrierBitmap == null){
                Drawable bI = getHiderDrawableImageId(hider);
                barrierBitmap = ((BitmapDrawable)bI).getBitmap();
            }

            RelativeLayout.LayoutParams barrierImageparms = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            //barrierImage.setPivotX(zero);
            //barrierImage.setPivotY(zero);
            barrierImage.setScaleX(densityScaleFactor);
            barrierImage.setScaleY(densityScaleFactor);
            barrierImage.setAdjustViewBounds(true);
            barrierImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
            float barrierPivotX = barrierImage.getPivotX();
            float barrierPivotY = barrierImage.getPivotY();
            if (hider.getSide() == 0) {
                barrierImageparms.leftMargin = x;
                barrierImage.setPivotX(zero);
            } else {
                barrierImageparms.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                barrierImageparms.rightMargin = x;
                barrierImage.setPivotX(barrierBitmap.getWidth());
            }
            barrierImage.setPivotY(barrierBitmap.getHeight());
            barrierImageparms.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            barrierImageparms.bottomMargin = (int)(y);
            barrierImage.setLayoutParams(barrierImageparms);
            barrierImage.setImageBitmap(barrierBitmap);


            ImageView characterImage = new ImageView(getApplicationContext());
            RelativeLayout.LayoutParams characterImageParams = new RelativeLayout.LayoutParams(195, 212);
            characterImage.setAdjustViewBounds(true);
            characterImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
            //creating Image and its bitmap of character Image
            characterImageX = x + (int)(barrierBitmap.getWidth()*densityScaleFactor/2) - (int)((195*hider.getScale()*densityScaleFactor) / 2);//characterBitmapForCalculation.getWidth()
            characterImageY = y - (int)(barrierBitmap.getHeight()/2) - (int)((212*hider.getScale())/ 2);
            if (hider.getSide() == 0) {
                characterImageParams.leftMargin = characterImageX;
                characterImage.setPivotX(zero);
            } else {
                characterImageParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                characterImageParams.rightMargin = characterImageX;
                characterImage.setPivotX(195);
            }
            //characterImage.setPivotY(0);
            characterImage.setPivotY(212);
            characterImageParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            characterImageParams.bottomMargin = y + (int)(10 *densityScaleFactor);
            characterImage.setScaleX(hider.getScale() * densityScaleFactor);
            characterImage.setScaleY(hider.getScale()* densityScaleFactor);
            //setting up character Image Margins
            characterImage.setLayoutParams(characterImageParams);
            CharacterState characterState = new CharacterState();
            characterImage.setTag(R.string.character_state_tag, characterState);
            layoutRootView.addView(characterImage);

            layoutRootView.addView(barrierImage);
            imagesList.add(characterImage);


        }
        Collections.reverse(imagesList);
    }

    private void setInitialScores(){
        score = findViewById(R.id.score);
        score.setText(String.valueOf(AppStateController.getInstance().getScore()));
        highScore = findViewById(R.id.highscore);
        highScore.setText(String.valueOf(sharedPreferences.getInt("highScore",zero)));
    }

    private void initializeTimer(){
        timer = new Timer();
        final Handler timerHandler = new Handler();

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                timerHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        timerClick();
                    }
                });
            }
        };
        timer.schedule(timerTask, timerDay, timerPeriod);
    }

    @Override
    protected void onPause() {
        myMediaPlayer.pause();
        mediaPlayerLenght = myMediaPlayer.getCurrentPosition();
        super.onPause();

    }

    @Override
    protected void onResume() {
        myMediaPlayer.seekTo(mediaPlayerLenght);

        if (sharedPreferences.getBoolean("isMusicOn",true)) {
            myMediaPlayer.start();
        }
        super.onResume();
    }

    private Scene selectScene() {
        ArrayList<Scene> scenesList = new ArrayList<Scene>();

        outerloop : for (Scene scene : AppStateController.getInstance().getElectionStore().getScenes()) {
            Bitmap sceneBgImage = BitmapFactory.decodeFile(getFilesDir().getAbsolutePath() + "/" + scene.getBackgroundImage());
             if (sceneBgImage == null) {
                 continue;
             }
             for (Hider hider: scene.getHiders()) {
                 Bitmap hiderImageBitmap = BitmapFactory.decodeFile(getFilesDir().getAbsolutePath() + "/" + hider.getImage());
                 if (hiderImageBitmap == null) {
                     continue outerloop;
                 }
             }
            scenesList.add(scene);

        }
        if (scenesList.size() == 0){
            return AppStateController.getInstance().getElectionStore().getScenes().get(0);
        }
             Random random = new Random();
            int sceneIndex = random.nextInt(scenesList.size());
            return scenesList.get(sceneIndex);

    }

    private  Drawable getHiderDrawableImageId(Hider hider) {
        String hiderImageName = hider.getImage().substring(0, hider.getImage().lastIndexOf("."));
        return getResources().getDrawable(getResources().getIdentifier(hiderImageName , "drawable", getPackageName()));
    }

    private void showAnimatedScore(String score , float X, float Y){
        LayoutParams lparams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        final TextView scoreTextView = new TextView(this);
        lparams.leftMargin = Math.round(X);
        lparams.topMargin =  Math.round(Y) ;
        scoreTextView.setLayoutParams(lparams);
        scoreTextView.setText(score);
        scoreTextView.setTextColor(getResources().getColor(R.color.white));
        scoreTextView.setTextSize(30 * AppStateController.getInstance().densityScaleFactor);
        layoutRootView.addView(scoreTextView);

        Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.textview_anim);
        scoreTextView.startAnimation(myAnim);
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(1f, 0f);
        valueAnimator.setDuration(1200);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float alpha = (float) animation.getAnimatedValue();
                scoreTextView.setAlpha(alpha);
            }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                layoutRootView.removeView(scoreTextView);
                //super.onAnimationEnd(animation);
            }
        });
        valueAnimator.start();

    }

    private void initPartyFlags(){
        ImageView party_logo = findViewById(R.id.party_logo);
        ImageView opponent_logo = findViewById(R.id.opponent_logo);
        party_logo.setImageDrawable(getLocalDrawableFlag(AppStateController.getInstance().getSelectedParty()));
        opponent_logo.setImageDrawable(getLocalDrawableFlag(AppStateController.getInstance().getOpponentParty()));

    }

    public Drawable getLocalDrawableFlag(Party party  ){
        String flagImageName = party.getFlag().substring(0, party.getFlag().lastIndexOf("."));
        return getResources().getDrawable(getResources().getIdentifier(flagImageName , "drawable", getPackageName()));
    }

    private void showInfoDialogBox(){
        LayoutInflater inflater = (LayoutInflater)this.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.dialog_activity, null);
        alertDialog = new AlertDialog.Builder(this)
                .setView(v)
                .create();
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
        TextView dialogTitle = (TextView)alertDialog.findViewById(R.id.title);
        TextView dialogScore = (TextView)alertDialog.findViewById(R.id.scoreTV);
        Button   dialogButton = (Button)alertDialog.findViewById(R.id.btn_close);
        if (AppStateController.getInstance().getScore() < 50){
            dialogTitle.setText("Try again");
        }
        dialogScore.setText("Score  " + String.valueOf(AppStateController.getInstance().getScore()));
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                endGame();
            }
        });

    }
    private void showHighScoreAnimation(String score ){
        LayoutParams lparams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        final TextView scoreTextView = new TextView(this);
        scoreTextView.setText(score);
        scoreTextView.setTextSize(50 * AppStateController.getInstance().densityScaleFactor);
        Rect bounds = new Rect();
        Paint textPaint = scoreTextView.getPaint();
        textPaint.getTextBounds(score, 0, score.length(), bounds);
        int textHeight = bounds.height();
        int textWidth = bounds.width();
        lparams.leftMargin = (screenWidth/2) - (textWidth/2);
        lparams.topMargin =  (screenHeight/2) - (textHeight/2);
        scoreTextView.setLayoutParams(lparams);
        scoreTextView.setTextColor(getResources().getColor(R.color.white));
        layoutRootView.addView(scoreTextView);

        Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.textview_anim);
        scoreTextView.startAnimation(myAnim);
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(1f, 0f);
        valueAnimator.setDuration(1500);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float alpha = (float) animation.getAnimatedValue();
                scoreTextView.setAlpha(alpha);
            }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                layoutRootView.removeView(scoreTextView);
                //super.onAnimationEnd(animation);
            }
        });
        valueAnimator.start();

    }

    public Drawable getMemberDrawableImage(String imagePath){
        String drawabeImagePath = imagePath.substring(0, imagePath.lastIndexOf("."));
        return getResources().getDrawable(getResources().getIdentifier(drawabeImagePath , "drawable", getPackageName()));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myMediaPlayer != null) {
            myMediaPlayer.release();
        }
        if ( alertDialog!=null && alertDialog.isShowing() ){
            alertDialog.dismiss();
        }
    }

}




