package com.ikslogics.siyasipunch.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ikslogics.siyasipunch.AppStateController;
import com.ikslogics.siyasipunch.R;
import com.ikslogics.siyasipunch.pojo.Party;
import com.ikslogics.siyasipunch.pojoWrappers.PartyWrapper;

import java.util.List;

/**
 * Created by khanzadabilal70 on 22/6/18.
 */

public class OpponentPartiesAdapter extends BaseAdapter
{
    int selectedPosition = 1 ;
    String selectedName;
    Context context;
    List<PartyWrapper> parties_List;
    LayoutInflater inflater;


    public OpponentPartiesAdapter(Context context, List<PartyWrapper> parties) {
        //super(context, parties);
        this.context = context;
        this.parties_List = parties;
        selectedName = parties_List.get(0).getParty().getFullName();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public void setSelectedName(String selectedName) {
        this.selectedName = selectedName;
    }


    @Override
    public int getCount() {
        return parties_List.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        view = inflater.inflate(R.layout.selection_view, parent, false);

        PartyWrapper partyWrapper = parties_List.get(position);
        ImageView party_image = view.findViewById(R.id.party_image);
        Bitmap partyBitmap = partyWrapper.getPartyFlag();
        if (partyBitmap == null){
            Drawable drawable = partyWrapper.getLocalDrawableFlag(context);
            partyBitmap = ((BitmapDrawable)drawable).getBitmap();
        }
        party_image.setImageBitmap(partyBitmap);

        if(selectedPosition == position){
            party_image.setImageBitmap(partyWrapper.getLocalSelectedFlagBitMap(context));
        } else {
            party_image.setImageBitmap(partyBitmap);
        }
        if (selectedName!= null && selectedName .equals(partyWrapper.getParty().getFullName())){
            view.setClickable(true);
            setViewEnabled(view,false);
        }

        return view;
    }

    private void setViewEnabled(View view, boolean isEnable) {

        view.setEnabled(isEnable);
        if (isEnable) {
            view.setAlpha(1f);
        } else {
            view.setAlpha(.5f);
        }

    }

}
