package com.ikslogics.siyasipunch.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.ikslogics.siyasipunch.AppStateController;
import com.ikslogics.siyasipunch.R;
import com.ikslogics.siyasipunch.pojo.Hider;
import com.ikslogics.siyasipunch.pojo.Party;
import com.ikslogics.siyasipunch.pojoWrappers.PartyWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by khanzadabilal70 on 21/6/18.
 */

public class PartiesAdapter extends BaseAdapter {
    Context context;
    List<PartyWrapper> parties_List;
    LayoutInflater inflater;
    int selectedPosition = 0 ;



    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }


    public PartiesAdapter(Context context , List<PartyWrapper> parties ) {
        this.context = context;
        this.parties_List = parties;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return parties_List.size();
    }

    @Override
    public Object getItem(int position) {
        return parties_List;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        view = inflater.inflate(R.layout.selection_view, parent, false);
        PartyWrapper partyWrapper = parties_List.get(position);
        ImageView party_image = view.findViewById(R.id.party_image);

        Bitmap partyBitmap = partyWrapper.getPartyFlag();
        if (partyBitmap == null){
            Drawable drawable = partyWrapper.getLocalDrawableFlag(context);
            partyBitmap = ((BitmapDrawable)drawable).getBitmap();
        }
        party_image.setImageBitmap(partyBitmap);
        if(selectedPosition == position){
            party_image.setImageBitmap(partyWrapper.getLocalSelectedFlagBitMap(context));
        } else {
            party_image.setImageBitmap(partyBitmap);
        }
        return view;
    }



}
