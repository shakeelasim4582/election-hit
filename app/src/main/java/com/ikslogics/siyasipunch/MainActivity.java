package com.ikslogics.siyasipunch;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.ikslogics.siyasipunch.pojo.BackgroundMusic;
import com.ikslogics.siyasipunch.pojo.ElectionStore;
import com.ikslogics.siyasipunch.pojo.Hider;
import com.ikslogics.siyasipunch.pojo.Image;
import com.ikslogics.siyasipunch.pojo.Member;
import com.ikslogics.siyasipunch.pojo.Party;
import com.ikslogics.siyasipunch.pojo.Poster;
import com.ikslogics.siyasipunch.pojo.Scene;

import java.io.File;
import java.io.IOException;

public class MainActivity extends Activity implements View.OnClickListener{


    ProgressBar progressBar;
    ImageButton gamePlayBtn;
    TextView highScore ;
    private AdView mAdView;
    SharedPreferences sharedPreferences;
    Switch sound_switch;
    Switch music_switch;
    LinearLayout childLayout;
    LinearLayout parentLayout;


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupFirbaseDatabase();
        findingViewsbyId();
        sharedPreferences = getSharedPreferences("election_hit", Activity.MODE_PRIVATE);
        MobileAds.initialize(this, getString(R.string.adMob_app_id));
        gamePlayBtn.setOnClickListener(this);

        showHighScore();
        setupSoundsSwitch();

        float deviceDensity = getResources().getDisplayMetrics().density;
        AppStateController.getInstance().densityScaleFactor = deviceDensity / AppStateController.BASE_DENSITY;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play:
                startActivity(new Intent(MainActivity.this, PartySelection.class));
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        showHighScore();
    }

    public void downloadResources() {

        for (Scene gameScene: AppStateController.getInstance().getElectionStore().getScenes()) {
            downloadFirebaseImage(gameScene.getBackgroundImage());
            for (Hider hider: gameScene.getHiders()) {
                downloadFirebaseImage(hider.getImage());
            }
        }

        for (Party party: AppStateController.getInstance().getElectionStore().getParties()) {
            downloadFirebaseImage(party.getFlag());
            for (Member partyMember: party.getMembers()) {
                for (Image memberImage: partyMember.getImages()) {
                    downloadFirebaseImage(memberImage.getImage());
                }
            }
            for (Poster poster: party.getPosters()) {
                downloadFirebaseImage(poster.getImage());
            }
        }

    }

    public void downloadFirebaseImage(String fileName) {

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();
        StorageReference imageRef = storageRef.child("/images/" + fileName);

        try {
            String path = getFilesDir().getAbsolutePath() + "/" + fileName;
            File localFile = new File(path);
            if (!localFile.exists() || localFile.length() == 0) {
                localFile.createNewFile();
                imageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        System.out.print(exception.getStackTrace());
                    }
                });
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void hideLoading(){
        parentLayout.setBackgroundResource(R.drawable.siyasipunch_bg);
        progressBar.setVisibility(View.GONE);
        parentLayout.getBackground().setAlpha(80);
        childLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        new AlertDialog.Builder(this)
                .setTitle("Exit Game ?")
                .setMessage("Do you really want to Exit ?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        int pid = android.os.Process.myPid();
                        android.os.Process.killProcess(pid);

                    }})
                .setNegativeButton(android.R.string.no, null).show();

    }

    private void setupSoundsSwitch(){

        final SharedPreferences.Editor editor = sharedPreferences.edit();

        if (sharedPreferences.getBoolean("isSoundOn",true)){
            sound_switch.setChecked(true);
        }
        sound_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    editor.putBoolean("isSoundOn", true);
                }
                else {
                    editor.putBoolean("isSoundOn", false);
                }
                editor.commit();

            }
        });

        if (sharedPreferences.getBoolean("isMusicOn",true)){
            music_switch.setChecked(true);
        }
        music_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    editor.putBoolean("isMusicOn", true);
                }else {
                    editor.putBoolean("isMusicOn", false);
                }
                editor.commit();

            }
        });
    }

    private void findingViewsbyId(){
        sound_switch = findViewById(R.id.sound_switch);
        music_switch = findViewById(R.id.music_switch);
        mAdView = findViewById(R.id.adView);
        progressBar = findViewById(R.id.progress_bar);
        gamePlayBtn = findViewById(R.id.play);
        highScore = findViewById(R.id.highScore);
        parentLayout = findViewById(R.id.parentLayout);
        childLayout = findViewById(R.id.childeLayout);

    }
    private void setupFirbaseDatabase(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        database.setPersistenceEnabled(true);
        DatabaseReference dbRef = database.getReference("electionStore");
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                AppStateController.getInstance().setElectionStore(dataSnapshot.getValue(ElectionStore.class));
                Log.e("election","Data retrieved");
                downloadResources();
                hideLoading();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("election", databaseError.getDetails());

            }
        });
    }

    private void showHighScore(){
        int highScoreValue = sharedPreferences.getInt("highScore", 0);
        highScore.setText(String.valueOf(highScoreValue));
    }
















}
