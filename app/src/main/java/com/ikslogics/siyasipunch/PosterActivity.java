package com.ikslogics.siyasipunch;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import com.ikslogics.siyasipunch.pojo.Party;
import com.ikslogics.siyasipunch.pojo.Poster;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PosterActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poster_activity);


        ImageView imageView = (ImageView)findViewById(R.id.poster_image);
        SharedPreferences sp = getSharedPreferences("election_hit", Activity.MODE_PRIVATE);
        List<Poster> posterLists = getPartyPosters();

        Random random = new Random();
        int randomPoster = random.nextInt(posterLists.size());
        Poster poster = posterLists.get(randomPoster);
        Bitmap posterBitmap = BitmapFactory.decodeFile(getFilesDir().getAbsolutePath() + "/" + poster.getImage());
        if (posterBitmap == null){
            endGame();

        }else {
            posterBitmap = drawTextToBitmap(posterBitmap);
            imageView.setImageBitmap(posterBitmap);
            sharePoster(posterBitmap);
            findViewById(R.id.play_again_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    endGame();
                }
            });
        }


    }

    public Bitmap drawTextToBitmap(Bitmap bitmap) {
            android.graphics.Bitmap.Config bitmapConfig =   bitmap.getConfig();
            // set default bitmap config if none
            if(bitmapConfig == null) {
                bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
            }
            // resource bitmaps are immutable,
            // so we need to convert it to mutable one
            bitmap = bitmap.copy(bitmapConfig, true);

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setColor(getResources().getColor(R.color.green));
            float densityScaleFactor = AppStateController.getInstance().densityScaleFactor;
            paint.setTextSize(getResources().getDimension(R.dimen.poster_score_size));
            paint.setShadowLayer(1f, 0f, 1f, Color.DKGRAY);

            DisplayMetrics displayMetrics = new DisplayMetrics();   //getting screen width and height
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            float deviceDensity =  displayMetrics.density;
            int screenHeight = displayMetrics.heightPixels;
            int screenWidth = displayMetrics.widthPixels;
            float x = 0;
            float y = 0 ;
            if (deviceDensity >=4){
                x = (18 *  screenWidth)/100;
                y = (1.7f * screenHeight)/100;

            }else if (deviceDensity >= 3.0){
                x = (38 *  screenWidth)/100;
                y = (2.3f * screenHeight)/100;

            }else if (deviceDensity >= 2.0){
                x = (60 *  screenWidth)/100;
                y = (3.1f * screenHeight)/100;

            }else if (deviceDensity >= 1.5){
                x = (58 *  screenWidth)/100;
                y = (2.7f * screenHeight)/100;
            }else if (deviceDensity >= 1.0){
                x = (57 *  screenWidth)/100;
                y = (3 * screenHeight)/100;
            }else {
                x = (50 *  screenWidth)/100;
                y = (3.4f * screenHeight)/100;
            }
            String text = "Score: " + String.valueOf(AppStateController.getInstance().getScore());
            canvas.drawText(text, x, y, paint);
            return bitmap;

    }

    private void sharePoster(Bitmap bitmap){
        final Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getContentResolver(),bitmap, "Election Hit Game", null);
        Uri imageUri =  Uri.parse(path);
        share.putExtra(Intent.EXTRA_STREAM, imageUri);
        share.putExtra(Intent.EXTRA_TEXT,"https://play.google.com/store/apps/details?id=com.ikslogics.siyasipunch");
        findViewById(R.id.share_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(Intent.createChooser(share, "select app to share"));
            }
        });
    }

    private List<Poster> getPartyPosters(){
        List<Poster> selectedPosters = new ArrayList<>();
        Party yourParty  = AppStateController.getInstance().getSelectedParty();
        Party opponentParty = AppStateController.getInstance().getOpponentParty();
        for(Poster poster : yourParty.getPosters()) {
            if (poster.getOpponentID() == opponentParty.getPID()){
                selectedPosters.add(poster);
            }
        }
        return selectedPosters;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void  endGame(){
        finish();
        startActivity(new Intent(PosterActivity.this,PartySelection.class));

    }

}

