
package com.ikslogics.siyasipunch.pojo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "hID",
    "image",
    "scale",
    "side",
    "x",
    "y"
})
public class Hider {

    @JsonProperty("hID")
    private Integer hID;
    @JsonProperty("image")
    private String image;
    @JsonProperty("scale")
    private float scale;
    @JsonProperty("side")
    private Integer side;
    @JsonProperty("x")
    private Integer x;
    @JsonProperty("y")
    private Integer y;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("hID")
    public Integer getHID() {
        return hID;
    }

    @JsonProperty("hID")
    public void setHID(Integer hID) {
        this.hID = hID;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    @JsonProperty("scale")
    public float getScale() {
        return scale;
    }
    @JsonProperty("scale")
    public void setScale(float scale) {
        this.scale = scale;
    }
    @JsonProperty("side")
    public Integer getSide() {
        return side;
    }
    @JsonProperty("side")
    public void setSide(Integer side) {
        this.side = side;
    }

    @JsonProperty("x")
    public Integer getX() {
        return x;
    }

    @JsonProperty("x")
    public void setX(Integer x) {
        this.x = x;
    }

    @JsonProperty("y")
    public Integer getY() {
        return y;
    }

    @JsonProperty("y")
    public void setY(Integer y) {
        this.y = y;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


}
