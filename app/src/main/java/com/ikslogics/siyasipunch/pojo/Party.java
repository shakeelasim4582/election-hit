
package com.ikslogics.siyasipunch.pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "pID",
    "fullName",
    "shortName",
    "logo",
    "flag",
    "sortOrder",
    "defaultOpponent",
    "members",
    "backgroundMusic",
    "posters"
})
public class Party {

    @JsonProperty("pID")
    private Integer pID;
    @JsonProperty("fullName")
    private String fullName;
    @JsonProperty("shortName")
    private String shortName;
    @JsonProperty("logo")
    private String logo;
    @JsonProperty("flag")
    private String flag;
    @JsonProperty("sortOrder")
    private Integer sortOrder;
    @JsonProperty("defaultOpponent")
    private Integer defaultOpponent;
    @JsonProperty("members")
    private List<Member> members = null;
    @JsonProperty("backgroundMusic")
    private List<BackgroundMusic> backgroundMusic = null;
    @JsonProperty("posters")
    private List<Poster> posters = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("pID")
    public Integer getPID() {
        return pID;
    }

    @JsonProperty("pID")
    public void setPID(Integer pID) {
        this.pID = pID;
    }

    @JsonProperty("fullName")
    public String getFullName() {
        return fullName;
    }

    @JsonProperty("fullName")
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @JsonProperty("shortName")
    public String getShortName() {
        return shortName;
    }

    @JsonProperty("shortName")
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @JsonProperty("logo")
    public String getLogo() {
        return logo;
    }

    @JsonProperty("logo")
    public void setLogo(String logo) {
        this.logo = logo;
    }

    @JsonProperty("flag")
    public String getFlag() {
        return flag;
    }

    @JsonProperty("flag")
    public void setFlag(String flag) {
        this.flag = flag;
    }

    @JsonProperty("sortOrder")
    public Integer getSortOrder() {
        return sortOrder;
    }

    @JsonProperty("sortOrder")
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @JsonProperty("defaultOpponent")
    public Integer getDefaultOpponent() {
        return defaultOpponent;
    }

    @JsonProperty("defaultOpponent")
    public void setDefaultOpponent(Integer defaultOpponent) {
        this.defaultOpponent = defaultOpponent;
    }

    @JsonProperty("members")
    public List<Member> getMembers() {
        return members;
    }

    @JsonProperty("members")
    public void setMembers(List<Member> members) {
        this.members = members;
    }

    @JsonProperty("backgroundMusic")
    public List<BackgroundMusic> getBackgroundMusic() {
        return backgroundMusic;
    }

    @JsonProperty("backgroundMusic")
    public void setBackgroundMusic(List<BackgroundMusic> backgroundMusic) {
        this.backgroundMusic = backgroundMusic;
    }

    @JsonProperty("posters")
    public List<Poster> getPosters() {
        return posters;
    }

    @JsonProperty("posters")
    public void setPosters(List<Poster> posters) {
        this.posters = posters;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
