
package com.ikslogics.siyasipunch.pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "parties",
    "scenes"
})
public class ElectionStore {

    @JsonProperty("parties")
    private List<Party> parties = null;
    @JsonProperty("scenes")
    private List<Scene> scenes = null;
    @JsonProperty("configurations")
    private Configurations configurations;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("parties")
    public List<Party> getParties() {
        return parties;
    }

    @JsonProperty("parties")
    public void setParties(List<Party> parties) {
        this.parties = parties;
    }

    @JsonProperty("scenes")
    public List<Scene> getScenes() {
        return scenes;
    }

    @JsonProperty("scenes")
    public void setScenes(List<Scene> scenes) {
        this.scenes = scenes;
    }
    @JsonProperty("configurations")
    public Configurations getConfigurations() {
        return configurations;
    }

    @JsonProperty("configurations")
    public void setConfigurations(Configurations configurations) {
        this.configurations = configurations;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
