
package com.ikslogics.siyasipunch.pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sID",
    "sName",
    "pID",
    "backgroundImage",
    "hiders"
})
public class Scene {

    @JsonProperty("sID")
    private Integer sID;
    @JsonProperty("sName")
    private String sName;
    @JsonProperty("pID")
    private Integer pID;
    @JsonProperty("backgroundImage")
    private String backgroundImage;
    @JsonProperty("hiders")
    private List<Hider> hiders = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("sID")
    public Integer getSID() {
        return sID;
    }

    @JsonProperty("sID")
    public void setSID(Integer sID) {
        this.sID = sID;
    }

    @JsonProperty("sName")
    public String getSName() {
        return sName;
    }

    @JsonProperty("sName")
    public void setSName(String sName) {
        this.sName = sName;
    }

    @JsonProperty("pID")
    public Integer getPID() {
        return pID;
    }

    @JsonProperty("pID")
    public void setPID(Integer pID) {
        this.pID = pID;
    }

    @JsonProperty("backgroundImage")
    public String getBackgroundImage() {
        return backgroundImage;
    }

    @JsonProperty("backgroundImage")
    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    @JsonProperty("hiders")
    public List<Hider> getHiders() {
        return hiders;
    }

    @JsonProperty("hiders")
    public void setHiders(List<Hider> hiders) {
        this.hiders = hiders;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
