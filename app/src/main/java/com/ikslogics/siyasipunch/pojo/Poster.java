
package com.ikslogics.siyasipunch.pojo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "posterID",
    "opponentID",
    "image",
    "imageX",
    "imageY",
    "label",
    "scoreX",
    "scoreY"
})
public class Poster {

    @JsonProperty("posterID")
    private Integer posterID;
    @JsonProperty("opponentID")
    private Integer opponentID;
    @JsonProperty("image")
    private String image;
    @JsonProperty("imageX")
    private Integer imageX;
    @JsonProperty("imageY")
    private Integer imageY;
    @JsonProperty("label")
    private String label;
    @JsonProperty("scoreX")
    private Integer scoreX;
    @JsonProperty("scoreY")
    private Integer scoreY;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("posterID")
    public Integer getPosterID() {
        return posterID;
    }

    @JsonProperty("posterID")
    public void setPosterID(Integer posterID) {
        this.posterID = posterID;
    }

    @JsonProperty("opponentID")
    public Integer getOpponentID() {
        return opponentID;
    }

    @JsonProperty("opponentID")
    public void setOpponentID(Integer opponentID) {
        this.opponentID = opponentID;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    @JsonProperty("imageX")
    public Integer getImageX() {
        return imageX;
    }

    @JsonProperty("imageX")
    public void setImageX(Integer imageX) {
        this.imageX = imageX;
    }

    @JsonProperty("imageY")
    public Integer getImageY() {
        return imageY;
    }

    @JsonProperty("imageY")
    public void setImageY(Integer imageY) {
        this.imageY = imageY;
    }

    @JsonProperty("label")
    public String getLabel() {
        return label;
    }

    @JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    @JsonProperty("scoreX")
    public Integer getScoreX() {
        return scoreX;
    }

    @JsonProperty("scoreX")
    public void setScoreX(Integer scoreX) {
        this.scoreX = scoreX;
    }

    @JsonProperty("scoreY")
    public Integer getScoreY() {
        return scoreY;
    }

    @JsonProperty("scoreY")
    public void setScoreY(Integer scoreY) {
        this.scoreY = scoreY;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
