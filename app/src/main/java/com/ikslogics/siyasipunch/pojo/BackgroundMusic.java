
package com.ikslogics.siyasipunch.pojo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "musicID",
    "music"
})
public class BackgroundMusic {

    @JsonProperty("musicID")
    private Integer musicID;
    @JsonProperty("music")
    private String music;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("musicID")
    public Integer getMusicID() {
        return musicID;
    }

    @JsonProperty("musicID")
    public void setMusicID(Integer musicID) {
        this.musicID = musicID;
    }

    @JsonProperty("music")
    public String getMusic() {
        return music;
    }

    @JsonProperty("music")
    public void setMusic(String music) {
        this.music = music;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
