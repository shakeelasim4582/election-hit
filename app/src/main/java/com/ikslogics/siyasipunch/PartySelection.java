package com.ikslogics.siyasipunch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.ikslogics.siyasipunch.adapters.OpponentPartiesAdapter;
import com.ikslogics.siyasipunch.adapters.PartiesAdapter;
import com.ikslogics.siyasipunch.pojo.Party;
import com.ikslogics.siyasipunch.pojoWrappers.PartyWrapper;

import java.util.ArrayList;

/**
 * Created by khanzadabilal70 on 21/6/18.
 */

public class PartySelection extends Activity implements View.OnClickListener{

    private AdView mAdView;
    Button done_btn;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.party_selection);

        ListView list_yourParty = findViewById(R.id.list_yourParty);
        ListView list_opponentParty =  findViewById(R.id.list_opponentParty);
        done_btn = findViewById(R.id.btn_play);
        mAdView = findViewById(R.id.adView);


        final ArrayList<PartyWrapper> parties = new ArrayList<PartyWrapper>();
        for (Party party: AppStateController.getInstance().getElectionStore().getParties()) {
            if (party.getPID() != 4 ) {
                PartyWrapper partyWrapper = new PartyWrapper(party);
                partyWrapper.populateBitmaps(this);
                parties.add(partyWrapper);
            }
        }

        final PartiesAdapter partiesAdapter= new PartiesAdapter(this,parties);
        final OpponentPartiesAdapter opponentPartiesAdapter = new OpponentPartiesAdapter(this,parties);
        Party selectedParty = AppStateController.getInstance().getSelectedParty();
        if (selectedParty == null){
            AppStateController.getInstance().setSelectedParty(parties.get(0).getParty());
        }else {
            partiesAdapter.setSelectedPosition(selectedPartyIndex(selectedParty));
            opponentPartiesAdapter.setSelectedName(selectedPartyName(selectedParty));
        }

        if (AppStateController.getInstance().getOpponentParty() == null){
            AppStateController.getInstance().setOpponentParty(parties.get(1).getParty());
        }else {
            opponentPartiesAdapter.setSelectedPosition(selectedPartyIndex(AppStateController.getInstance().getOpponentParty()));
        }

        list_yourParty.setAdapter(partiesAdapter);
        list_yourParty.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Party party = AppStateController.getInstance().getElectionStore().getParties().get(position);
                partiesAdapter.setSelectedPosition(position);
                AppStateController.getInstance().setSelectedParty(party);
                Party opponent_Party = AppStateController.getInstance().getOpponentParty();
                if (opponent_Party != null && party.getPID() == opponent_Party.getPID()){
                    AppStateController.getInstance().setOpponentParty(null);
                    opponentPartiesAdapter.setSelectedPosition(-1);
                }
                partiesAdapter.notifyDataSetInvalidated();
                opponentPartiesAdapter.setSelectedName(party.getFullName());
                opponentPartiesAdapter.notifyDataSetInvalidated();
            }
        });

        list_opponentParty.setAdapter(opponentPartiesAdapter);
        list_opponentParty.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Party party = AppStateController.getInstance().getElectionStore().getParties().get(position);
                AppStateController.getInstance().setOpponentParty(party);
                opponentPartiesAdapter.setSelectedPosition(position);
                opponentPartiesAdapter.notifyDataSetInvalidated();

            }
        });

        done_btn.setOnClickListener(this);

        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        LinearLayout parentLayout  = (LinearLayout) findViewById(R.id.parentLayout);
        parentLayout.getBackground().setAlpha(100);

    }

    @Override
    public void onClick(View v) {
        if (AppStateController.getInstance().getOpponentParty() == null  ){
            Toast toast = Toast.makeText(this,"Please Select your Opponent", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

        }

        else {
            finish();
            startActivity(new Intent(PartySelection.this, GameActivity.class));
        }
    }

    private int selectedPartyIndex(Party party){
        for (int i = 0 ;i < AppStateController.getInstance().getElectionStore().getParties().size();i++){
            if (AppStateController.getInstance().getElectionStore().getParties().get(i).getPID() == party.getPID()){
                return i;
            }
        }
        return 0;
    }
    private String selectedPartyName(Party party){
        for (Party p:AppStateController.getInstance().getElectionStore().getParties()) {
            if (p.getPID() == party.getPID()){
                return p.getFullName();
            }

        }
        return "";
    }

}
