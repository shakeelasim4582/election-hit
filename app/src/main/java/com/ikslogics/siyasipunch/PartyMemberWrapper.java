package com.ikslogics.siyasipunch;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;

import com.ikslogics.siyasipunch.pojo.Image;
import com.ikslogics.siyasipunch.pojo.Member;

import java.util.ArrayList;

public class PartyMemberWrapper {
    private Member memberPojo;
    private ArrayList bitmapImages = new ArrayList<Bitmap>();
    int hitCount = 0;
    private int partyId;

    public PartyMemberWrapper(Member member) {
        this.memberPojo = member;

    }

    public void populateBitmaps(Context context) {
        for (Image image:memberPojo.getImages()) {
            Bitmap characterBitmap = BitmapFactory.decodeFile(context.getFilesDir().getAbsolutePath() + "/" + image.getImage());
            bitmapImages.add(characterBitmap);
        }
    }

    public Member getMemberPojo() {
        return memberPojo;
    }

    public void setMemberPojo(Member memberPojo) {
        this.memberPojo = memberPojo;
    }

    public ArrayList getBitmapImages() {
        return bitmapImages;
    }

    public int getPartyId() {
        return partyId;
    }

    public void setPartyId(int partyId) {
        this.partyId = partyId;
    }

    public int getHitCount() {
        return hitCount;
    }

    public void setHitCount(int i) {
        hitCount = hitCount+i;
    }



}
