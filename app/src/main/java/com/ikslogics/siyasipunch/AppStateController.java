package com.ikslogics.siyasipunch;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.ikslogics.siyasipunch.pojo.ElectionStore;
import com.ikslogics.siyasipunch.pojo.Party;

import java.util.Calendar;

/**
 * Created by KhanzadaBilal70 on 26/6/18.
 */

public class AppStateController {



    private static final AppStateController ourInstance = new AppStateController();

    private ElectionStore electionStore;
    private Party selectedParty ;
    private Party opponentParty ;
    int score = 0;
    int partyHitCount = 3;
    int highScore = 0;
    float densityScaleFactor;
    public static final float BASE_DENSITY =2f;


    public static AppStateController getInstance() {
        return ourInstance;
    }

    private AppStateController() {

    }

    public ElectionStore getElectionStore() {
        return electionStore;
    }

    public void setElectionStore(ElectionStore electionStore) {
        this.electionStore = electionStore;
    }
    public Party getSelectedParty() {
        return selectedParty;
    }

    public void setSelectedParty(Party selectedParty) {
        this.selectedParty = selectedParty;
    }

    public void setScore(int i) {
        score = score+i;
    }

    public Party getOpponentParty() {
        return opponentParty;
    }

    public void setOpponentParty(Party opponentParty) {
        this.opponentParty = opponentParty;
    }

    public int getScore() {
        return score;
    }

    public int getPartyHitCount() {
        return partyHitCount;
    }

    public void setPartyHitCount(int i) {
        partyHitCount = partyHitCount + i;

    }

    public int getHighScore() {
        return highScore;
    }

    public void setHighScore(int highScore) {
        this.highScore = highScore;
    }

    public boolean getMusicState(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("election_hit", Activity.MODE_PRIVATE);
        boolean value = sharedPreferences.getBoolean("isMusicOn",false);
        return value;
    }

}
